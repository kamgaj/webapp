How to prepare azure virtual machines that you can use with my repository:  

Server:  
1 login or create azure account here: https://portal.azure.com/#home  
2 choose virtual machines -> azure virtual machine  
3 create new resource group  
4 name your virtual machine  
5 choose region (the cheapest one is (Asia Pacific) South India)  
6 don't change availability options nor security type  
7 select image Ubuntu Server 20.04 LTS - Gen2  
8 don't mark Azure Spot instance  
9 select size: Standard_B1s  
10 select SSH public key  
11 set username to ubuntu  
12 leave generate new key pair  
13 name the SSH public key (i will be using name: ssh in commands)  
14 allow selected ports: SSH (22) and HTTP (80)  
15 select Review + create  
16 Download private key and create resource  

Server2:  
1 repeat steps 1-11 from Server creation  
2 select: use exisiting key stored in azure  
3 select key that you generated while creating server  
4 repeat steps 14-15 from Server creation  
5 confirm and create machine  

How to use program on server:  
If you are using windows I highly recommend installing git bash: https://git-scm.com/downloads  

1 copy ssh key using linux cmd or git bash: cp ./ssh.pem ~/.ssh/  
2 use command: chmod 600 ~/.ssh/ssh.pem  
3 start agent: eval "$(ssh-agent -s)"  
4 add key: ssh-add ~/.ssh/ssh.pem  
5 in order to connect to your server copy your server IP address and type: ssh ubuntu@serverIPaddress  
6 execute commands:  
sudo mkdir /var/www  
cd /var/www  
sudo chown -R ubuntu:ubuntu /var/www/  
7 clone my repository  
8 install packages using commands:  
curl -sL https://deb.nodesource.com/setup_16.x | sudo bash -  
sudo apt-get update  
sudo apt-get install -y nodejs nginx  
sudo npm install -g pm2  
node -v  
pm2 -v  
nginx -v  
9 modify nginx configuration:  
sudo nano -n /etc/nginx/nginx.conf  
10 copy paste into nginx.conf:  

user www-data;  
worker_processes auto;  
pid /run/nginx.pid;  
include /etc/nginx/modules-enabled/*.conf;  

events {  
        worker_connections 768;  
}  

http {  
        sendfile on;  
        tcp_nopush on;  
        tcp_nodelay on;  
        keepalive_timeout 65;  
        types_hash_max_size 2048;  
        server_tokens off;  

        include /etc/nginx/mime.types;
        default_type application/octet-stream;
        access_log /var/log/nginx/access.log;
        error_log /var/log/nginx/error.log;
        gzip on;
        server {
        listen 80;
                root /var/www/webapp/front/dist;
                index index.html;

        location /api {
                proxy_pass http://localhost:3000;
        }
        location /api/socket {
        proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_set_header Host $http_host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

                proxy_set_header X-Frame-Options SAMEORIGIN;
                proxy_pass http://localhost:3000;
        }
}  
}  

11 press ctrl+X -> Y -> enter  
12 restart nginx:  
sudo systemctl restart nginx  
sudo systemctl status nginx  
13 configure access data:  
nano /var/www/webapp/config.js  
14 put your login, password and agent number  
15 press ctrl+X -> Y -> enter  
16 to test application on server execute:  
cd /var/www/webapp/front  
npm ci  
npm run build  
cd /var/www/webapp  
npm ci  
pm2 start index.js  
17 to check backend status: pm2 status  
18 put your server IP in web browser window  
19 enjoy  

How to transfer program from server to server2 using ansible:  
1 if you didn't use program on server, copy ssh key using linux cmd or git bash: cp ./ssh.pem ~/.ssh/  
2 if you didn't use program on server, use command: chmod 600 ~/.ssh/ssh.pem  
3 start agent: eval "$(ssh-agent -s)"  
4 add key: ssh-add ~/.ssh/ssh.pem  
5 connect to your server: ssh ubuntu@serverIPaddress  
6 terminate connection: exit  
7 copy ssh key to server:  
scp ~/.ssh/ssh.pem ubuntu@serverIPaddress:~/.ssh/ssh.pem  
8 connect to server: ssh ubuntu@serverIPaddress  
9 use command: chmod 600 ~/.ssh/ssh.pem  
10 execute:  
eval "$(ssh-agent -s)"  
ssh-add ~/.ssh/ssh.pem  
11 connect to your server2: ssh ubuntu@server2IPaddress  
12 terminate connection: exit  
13 clone my repository  
14 configure access data:  
nano webapp/config.js  
15 repeat steps 14-15 from using program on server  
16 install ansible:  
sudo apt update  
sudo apt install -y ansible  
ansible --version  
17 go to ansible directory: cd webapp/ansible  
18 edit inventory: nano inventory  
19 below [azure] change ip address to your server2 ip address  
20 press ctrl+X -> Y -> enter  
21 to transfer application to server2 execute:  
ansible-playbook -K -e "target=azure" -i inventory install.yml  
22 enter your root password  
23 put your server2 IP in web browser window  
24 enjoy